<?php include 'header_0.php' ?>
<!-- Main -->

<section id="login">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-push-4 col-sm-5 col-sm-push-7">
                <h3><strong><span class="fa fa-fw fa-user-circle fa-lg"></span> Log In</strong></h3>
                <br>
                <p style="color:#ff6666"> <?php echo $this->session->flashdata('pesan') ?></p>
                <form action="<?php echo base_url('index.php/user/aksi_login') ?>" method="post">
                    <fieldset>
                        <div class="form-group">
                            <label for="username">Email</label>
                            <div class="input-group">
                              <div class="input-group-addon"><span class="fa fa-envelope" aria-hidden="true"></span></div>
                              <input type="email" class="form-control" name="email"  placeholder="Email">
                          </div>
                      </div>
                      <div class="form-group">
                        <label for="password">Password</label>
                        <div class="input-group">
                          <div class="input-group-addon"><span class="fa fa-key" aria-hidden="true"></span></div>
                          <input type="password" class="form-control" name="password" placeholder="Password">
                      </div>
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-af btn-block" name="button">Log In</button>
                </div><div class="form-group">
                    <a href="<?php echo base_url('user/daftar') ?>" type="button" class="btn btn-af" name="button">Daftar</a>
                    
                </div>
            </fieldset>
        </form>
    </div>
</div>
</div>
</section>
<!-- End Main -->
<?php include 'footer.php' ?>
