<?php include 'header_1.php'; ?>
<!-- HEADER -->
<header id="main-header">
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <h2><span class="lnr lnr-user" ></span> User</small></h2>
      </div>
    </div>
  </div>
</header>
<!-- END HEADER -->
<!-- MAIN -->
<section id="main-content">
  <div class="container">
    <div class="row">
      <?php include 'sidebar_menu.php'; ?>
      <div class="col-md-9">
        <div class="row">
          <div class="col-md-12">
            <!-- Form Edit Event -->
            <div class="panel panel-default" id="table-tna">
              <div class="panel-heading main-color-bg">
                <h3 class="panel-title">Edit Users</h3>
              </div>
              <div class="panel-body">
                <p style="color:#ff6666"> <?php echo $this->session->flashdata('pesan') ?></p>
                <?php 
                foreach($data as $d) 
                {
                  echo form_open_multipart('dashboard/edit_profile' ,'id="registerForm"');

                  ?>
                  <div class="row">
                    <!-- <div class="col-sm-12">
                      <img style="margin: auto; margin-bottom: 10px; width: 150px;height: 200px;" src="<?php echo base_url().$d['image'] ?>"alt="Foto" class="img-responsive foto-profil">
                    </div> --> 
                  </div>
                  <div class="row" >
                    <div class="col-sm-6 form-horizontal" >
                      <div class="form-group">
                        <label for="inputNamaLengkap" class="col-sm-4 control-label">Nama Lengkap</label>
                        <div class="col-sm-8">
                          <input type="hidden" name="id" class="form-control" value="<?php echo $d['id'];?>">
                          <input type="text" class="form-control" id="inputNamaLengkap" name="username" value="<?php echo $d['username'] ?>" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEmail" class="col-sm-4 control-label">Alamat Email</label>
                        <div class="col-sm-8">
                          <input type="email" class="form-control" id="inputEmail" name="email" value="<?php echo $d['email'] ?>"required>
                        </div>
                      </div>
                      

                      <div class="form-group">
                        <label for="inputEmail" class="col-sm-4 control-label">Profile Image</label>
                        <div class="col-sm-8">
                          <input type="hidden" name="filelama" class="form-control" value="<?php echo $d['profile_image'];?>">
                          <input type="file"  class="form-control" name="userfile">
                        </div>
                      </div>
                      <hr>
                    </div>
                    <div class="col-sm-6 form-horizontal">

                      <img src="<?php echo base_url()?>file_upload/<?php echo $d['profile_image'] ?>" alt="Foto" class="img-responsive foto-profil" width="100%" height="100%">

                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <button type="button" onclick="goBack()" class="btn btn-default">Back</button>
                        <button type="submit" name="submit" class="btn btn-primary pull-right">Submit</button>
                        <!-- <button type="button" class="btn btn-primary" id="btn-createJenjang" name="btn-createJenjang" ><span class="lnr lnr-pencil">Edit Password</span></button> -->
                      </div>
                    </div>
                  </div>
                  <?php
                }
                form_close();

                ?>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- END MAIN -->
      <?php include 'footer.php'; ?>
