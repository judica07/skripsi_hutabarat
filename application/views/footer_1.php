<!-- JS ASSETS -->
<script src="<?php echo base_url() ?>assets/jquery/jquery-3.2.1.min.js" charset="utf-8"></script>
<script src="<?php echo base_url() ?>assets/bootstrap/js/bootstrap.min.js" charset="utf-8"></script>
<script src="<?php echo base_url() ?>assets/scrolling-nav/jquery.easing.min.js" charset="utf-8"></script>
<script src="<?php echo base_url() ?>assets/scrolling-nav/scrolling-nav.js" charset="utf-8"></script>
<script src="<?php echo base_url() ?>assets/jquery-validation/jquery.validate.min.js" charset="utf-8"></script>
<script src="<?php echo base_url() ?>assets/jquery-validation/localization/messages_id.min.js" charset="utf-8"></script>
<script src="<?php echo base_url() ?>assets/DataTables/datatables.min.js" charset="utf-8"></script>
 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?php echo base_url() ?>assets/metisMenu/metisMenu.js" charset="utf-8"></script>
<script src="https://cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>

  <script type="text/javascript" src="<?php echo base_url()?>assets/formvalidator/jquery.form-validator.js"></script>
  
  
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBlTfW9u418uYgl6VCtNbdHJZRIQbxs8Pc&callback=initMap">
    </script>
    <script>
    $( function() {
      $("#registerForm").validate({
      rules: {
        password: {
          minlength: 5
        },
        confirm_password: {
          minlength: 5,
          equalTo: "#password"
        }
      }
    });
      // $( "#datepicker" ).datepicker();
      $.validate({
      validateOnBlur : false,
        errorMessagePosition : 'top'
    });

    var currentTime = new Date();
      var day = currentTime.getDate();
      var month = currentTime.getMonth() + 1;
      var year = currentTime.getFullYear();
    
     $("#tgl_mulai").datepicker({
        maxDate : "3m",
          minDate: new Date(year, month - 1, day),
         dateFormat: "yy-mm-dd",
        onSelect: function() {
            var minDate = $('#tgl_mulai').datepicker('getDate');
            var d = minDate.getDate();
            var m = minDate.getMonth();
          var y = minDate.getFullYear();
            $("#tgl_akhir").datepicker("change", { minDate: new Date(y, m, d+1) });
        }
    });

    $("#tgl_akhir").datepicker({
        maxDate : "3m",
        dateFormat: "yy-mm-dd"
    });

    } );

    
  </script>
<script type="text/javascript">
  function goBack(){
    window.history.back();
  };

  $(document).ready(function(){
    $('#sidebar-menu').metisMenu();

    
    $("#table-users").DataTable({
          scrollX:true,
          "aoColumnDefs": [
          {
            "bSortable": false,
            "aTargets": [ -1 ]
          }
          ]
        });
    $("#form-datapribadi").validate();
    $("#form-ubahpassword").validate({
      rules: {
       password_baru: {
        minlength: 5
      },
      confirm_password_baru: {
        minlength: 5,
        equalTo: "#password_baru"
      }
    }
  });


    $("#form-akunbank").validate();

    $("#form-createLead-1").validate();
    $("#table-lead-1").DataTable({
      "scrollX": true,
      "aoColumnDefs": [
      {
        "bSortable": false,
        "aTargets": [ -1 ]
      }
      ]
    });
    $("#table-linkProduk-1").DataTable({
      "scrollX": true,
      "aoColumnDefs": [
      {
        "bSortable": false,
        "aTargets": [ -1 ]
      }
      ]
    });
    $("#table-marketingTools-1").DataTable({
      "scrollX": true,
      "aoColumnDefs": [
      {
        "bSortable": false,
        "aTargets": [ -1 ]
      }
      ]
    });
    $("#btn-createJenjang").click(function(){
          $("#table-jenjang").hide();
          $("#form-createJenjang").fadeIn(500);
        });
    $("#btn-createLead").click(function(){
      $("#table-lead").hide();
      $("#form-createLead").fadeIn(500);
      $("#event").focus();
    });
    $("#btn-createLeadCancel").click(function(){
      $("#form-createLead").hide();
      $("#table-lead").fadeIn(500);
      $("#event").val("");
      $("#nama").val("");
      $("#email").val("");
      $("#telepon").val("");
      $("#pekerjaan").val("");
      $("#jabatan").val("");
      $("#perusahaan").val("");
      $("#dp").val("");
      $("#angsuran").val("");
    });
    $("#btn-createLeadReset").click(function(){
      $("#event").focus();
    });

    $("#form-editLead-1").validate();
    $("#table-komisi-1").DataTable();
    $("#table-transaksi-1").DataTable({
      scrollX: true
    });
    $("#table-top-10").DataTable();
    $("#form-createTna-1").validate();
    $("#form-editTna-1").validate();
    $("#table-maketingTools-1").DataTable();
    $("#table-tutorial-1").DataTable();
    $("#table-faq-1").DataTable();
    $("#form-pendaftaran").validate();
  });
  $("#table-event-1").DataTable({
          "scrollX": true,
          "aoColumnDefs": [
          {
            "width": "150px",
            "bSortable": false,
            "aTargets": [ -1 ]
          }
          ]
        });
</script>
</body>
</html>
