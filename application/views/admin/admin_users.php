<?php include 'header.php';?>
<!-- HEADER -->
<header id="main-header">
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <h2><span class="lnr lnr-user" ></span> Users</h2>
      </div>
    </div>
  </div>
</header>
<!-- END HEADER -->
<!-- MAIN -->
<section id="main-content">
  <div class="container">
    <div class="row">
      <?php include 'sidebar.php';?>
      <div class="col-md-9">
        <div class="row">
          <!-- Users -->
          <div class="col-md-12">
            <div id="table-users-1" class="panel panel-default">
              <div class="panel-heading main-color-bg">
                <h3 class="panel-title">Users</h3>
              </div>
              <div class="panel-body">
                <table id="table-users" class="table table-striped table-bordered" width="100%">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Username</th>
                      <th>Nama</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $x=1; foreach ($data as $d) {?>
                      <tr>
                        <td><?php echo $x ?></td>
                        <td><?php echo $d['email']?></td>
                        <td><?php echo $d['nama_user']?></td>
                        
                        <td>
                          <a href="<?php echo base_url('admin/edit_users/').$d['id'];?>" class="btn btn-default btn-sm"><span class="lnr lnr-pencil">Edit</span> </a>
                          <a href="<?php echo base_url('admin/edit_pass/').$d['id'];?>" class="btn btn-default btn-sm"><span class="lnr lnr-pencil">Edit Password</span> </a>
                        </td>
                      </tr>
                      <?php $x++;};?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- END MAIN -->
    <?php include 'footer.php'; ?>
