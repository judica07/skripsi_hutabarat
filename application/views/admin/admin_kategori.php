<?php include 'header.php'; ?>
<!-- HEADER -->
<header id="main-header">
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <h2><span class="lnr lnr-bullhorn" ></span>Kategori</h2>
      </div>
    </div>
  </div>
</header>
<!-- END HEADER -->
<!-- MAIN -->
<section id="main-content">
  <div class="container">
    <div class="row">
      <?php include 'sidebar.php'; ?>
      <div class="col-md-9">
        <div class="row">
          <div class="col-md-12">
            <!-- Events -->
            <div class="panel panel-default" id="table-event">
              <div class="panel-heading main-color-bg">
                <h3 class="panel-title">Kategori</h3>
                <a href="<?php echo base_url('admin/kategori_form')?>" class="btn btn-default btn-xs pull-right btn-create"><span class="fa fa-plus" aria-hidden="true"></span> </a>
              </div>
              <div class="panel-body">
                <p style="color:#ff6666"> <?php echo $this->session->flashdata('pesan') ?></p>
                <table id="table-event-1" class="table table-striped table-bordered" width="100%">
                  <thead>
                    <tr>
                      <th>NO</th>
                      <th>ID Kategori</th>
                      <th>Nama Ketegori</th>              
                    </tr>
                  </thead>
                  <tbody>
                    <?php $x=1; foreach ($data as $d) {?>
                      <tr>
                        <td><?php echo $x ?></td>
                        <td><?php echo $d['id_kategori']?></td>
                        <td><?php echo $d['nama_kategori']?></td>                        
                        <td>
                          <a href="<?php echo base_url('admin/kategori/').$d['id_kategori'];?>" class="btn btn-default btn-sm"><span class="lnr lnr-pencil">Edit</span> </a>
                          <a href="<?php echo base_url('admin/hapus_kategori/').$d['id_kategori'];?>" onclick="return confirm('Yakin Ingin Menghapus Gerai ini ?')" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-remove">Hapus</span> </a>                          
                        </td>
                      </tr>
                      <?php $x++;};?>
                    </tbody>

                  </table>
                </div>
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- END MAIN -->
    <?php include 'footer.php'; ?>
