<?php include 'header.php'; ?>
<!-- HEADER -->
<header id="main-header">
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <h2><span class="lnr lnr-cog" ></span> Dashboard</h2>
      </div>
    </div>
  </div>
</header>
<!-- END HEADER -->
<!-- MAIN -->
<section id="main-content">
  <div class="container">
    <div class="row">
      <?php include 'sidebar.php';?>
      <div class="col-md-9">
        <div class="row">
          <!-- Overview -->
          <div class="col-md-12">
            <div class="panel panel-default">
              <div class="panel-heading main-color-bg">
                <h3 class="panel-title">Overview</h3>
              </div>
              <div class="panel-body">
                <div class="col-md-4 box">
                 <a href="<?php echo base_url('admin/users') ?>"> 
                   <div class="well overview-box">
                    <?php foreach ($user as $u){};?>
                    <h3><span class="lnr lnr-user" aria-hidden="true"></span> <?php $jumlah = $this->db->get('user')->num_rows(); echo $jumlah; ?></h3>
                    <h5>Users</h5>
                  </div>
                </a>
              </div>
              <div class="col-md-4 box">
                <a href="<?php echo base_url('admin/gerai') ?>"> 
                  <div class="well overview-box">
                    <!-- <?php foreach ($gerai as $g){};?> -->
                    <h3><span class="lnr lnr-bullhorn" aria-hidden="true"></span> <?php $jumlah = $this->db->get('gerai')->num_rows(); echo $jumlah; ?></h3>
                    <h5>Gerai</h5>
                  </div>
                </a>
              </div>
              <div class="col-md-4 box">
                <a href="<?php echo base_url('admin/barang') ?>"> 
                  <div class="well overview-box">
                    <!-- <?php foreach ($barang as $b){};?> -->
                    <h3><span class="lnr lnr-file-add" aria-hidden="true"></span> <?php $jumlah = $this->db->get('barang')->num_rows(); echo $jumlah; ?></h3>
                    <h5>Barang</h5>
                  </div>
                </a>
              </div>
            </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- END MAIN -->
  <?php include 'footer.php'?>
