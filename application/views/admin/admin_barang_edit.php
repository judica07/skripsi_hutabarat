<?php include 'header.php'; ?>
<!-- HEADER -->
<header id="main-header">
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <h2><span class="lnr lnr-bullhorn" ></span> Barang Edit</h2>
      </div>
    </div>
  </div>
</header>
<!-- END HEADER -->
<!-- MAIN -->
<section id="main-content">
  <div class="container">
    <div class="row">
      <?php include 'sidebar.php'; ?>
      <div class="col-md-9">
        <div class="row">
          <div class="col-md-12">
            <!-- Form Edit Event -->
            <div class="panel panel-default" id="table-tna">
              <div class="panel-heading main-color-bg">
                <h3 class="panel-title">Barang Edit</h3>
              </div>
              <div class="panel-body">
                <p style="color:#ff6666"> <?php echo $this->session->flashdata('pesan') ?></p>
                <?php foreach ($data as $d){}?>
                <?php
                echo form_open_multipart('admin/barang_edit');
                ?>
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">Nama Barang</label>
                    <div class="col-sm-10">
                      <input type="hidden" name="id" value="<?php echo $d['id_barang']?>">
                      <input type="text" class="form-control" id="inputEvent" name="nama_barang" value="<?php echo $d['nama_barang']?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">Harga Barang</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputEvent" name="harga_barang" value="<?php echo $d['harga_barang']?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">Harga Promo</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputEvent" name="harga_promo" value="<?php echo $d['harga_promo']?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">Image</label>
                    <div class="col-sm-10">
                      <input type="hidden" name="filelama" class="form-control" value="<?php echo $d['image'];?>">
                          <input type="file"  class="form-control" name="image">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">Keterangan </label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputEvent" name="keterangan1" value="<?php echo $d['keterangan']?>">
                    </div>
                  </div>
                   <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">Kategori</label>
                    <div class="col-sm-10">
                      <select class="form-control" id="event" name="id_kategori">
                          <?php foreach ($kategori as $k ) {
                            if ($d['kategori_id']==$k['id_kategori']){?>
                            <option value="<?php echo $k['id_kategori'];?>" selected><?php echo $k['nama_kategori']?></option>
                            <?php } else {?>
                            <option value="<?php echo $k['id_kategori'];?>"><?php echo $k['nama_kategori']?></option>
                            <?php }};?>
                          </select> 
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">mulai Promo</label>
                    <div class="col-sm-10">
                      <input type="text" name="tgl_mulai" id="tgl_mulai" size ="15" data-validation="date" data-validation-format="yyyy-mm-dd" class="form-control" placeholder="Tanggal Mulai" value="<?php echo $d['mulai']?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">akhir Promo</label>
                    <div class="col-sm-10">
                      <input type="text" name="tgl_akhir" id="tgl_akhir" size ="15" data-validation="date" data-validation-format="yyyy-mm-dd" class="form-control" placeholder="Tanggal akhir" value="<?php echo $d['akhir']?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <div class="pull-right">
                        <a class="btn btn-danger" href="<?php echo base_url('index.php/admin/barang');?>">Back</a>
                        <button type="submit" class="btn btn-primary" name="submit">Submit</button>
                      </div>
                    </div>
                  </div>
                <?php
                      form_close(); 
                      ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- END MAIN -->
  <?php include 'footer.php'; ?>
