<?php include 'header.php'; ?>
<!-- HEADER -->
<header id="main-header">
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <h2><span class="lnr lnr-bullhorn" ></span> Barang</h2>
      </div>
    </div>
  </div>
</header>
<!-- END HEADER -->
<!-- MAIN -->
<section id="main-content">
  <div class="container">
    <div class="row">
      <?php include 'sidebar.php'; ?>
      <div class="col-md-9">
        <div class="row">
          <div class="col-md-12">
            <!-- Form Edit Event -->
            <div class="panel panel-default" id="table-tna">
              <div class="panel-heading main-color-bg">
                <h3 class="panel-title">Tambah Barang</h3>
              </div>
              <div class="panel-body">
                <p style="color:#ff6666"> <?php echo $this->session->flashdata('pesan') ?></p>
                <?php foreach ($data as $d){}?>
                <form class="form-horizontal" method="post" action="<?php echo base_url('admin/tambah_barang/').$d['market_id'];?>" enctype="multipart/form-data">
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">Nama barang</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputEvent" name="nama_barang" >
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">Harga Barang</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputEvent" name="harga_barang" >
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">Harga Promo</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputEvent" name="harga_promo" >
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">Gambar</label>
                    <div class="col-sm-10">
                      <input type="file" class="form-control" id="foto_barang" name="foto_barang" placeholder="Foto Profil" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">Keterangan</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputEvent" name="keterangan1" >
                    </div>
                  </div>
                  <input type="text" hidden="hidden" name="day" id="this-day" size ="15" data-validation="date">
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">Tanggal mulai Promo</label>
                    <div class="col-sm-10">
                      <input type="text" name="tgl_mulai" id="tgl_mulai" size ="15" data-validation="date" data-validation-format="yyyy-mm-dd" class="form-control" placeholder="Tanggal Mulai" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">Tanggal akhir promo</label>
                    <div class="col-sm-10">
                      <input type="text" name="tgl_akhir" id="tgl_akhir" size ="15" data-validation="date" data-validation-format="yyyy-mm-dd" class="form-control" placeholder="Tanggal Akhir" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">Kategori</label>
                    <div class="col-sm-10">
                      <select class="form-control" id="event" name="id_kategori">
                          <?php foreach ($kategori as $k ) {
                             {?>
                            <option value="<?php echo $k['id_kategori'];?>"><?php echo $k['nama_kategori']?></option>
                            <?php }};?>
                          </select> 
                    </div>
                  </div>                                                      
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <div class="pull-right">
                        <a class="btn btn-danger" href="<?php echo base_url('index.php/admin/gerai');?>">Back</a>
                        <button type="submit" class="btn btn-primary" name="submit">Submit</button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- END MAIN -->
  <?php include 'footer.php'; ?>
