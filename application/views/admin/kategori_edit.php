<?php include 'header.php'; ?>
<!-- HEADER -->
<header id="main-header">
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <h2><span class="lnr lnr-user" ></span>Kategori</small></h2>
      </div>
    </div>
  </div>
</header>
<!-- END HEADER -->
<!-- MAIN -->
<section id="main-content">
  <div class="container">
    <div class="row">
      <?php include 'sidebar.php'; ?>
      <div class="col-md-9">
        <div class="row">
          <div class="col-md-12">
            <!-- Form Edit Event -->
            <div class="panel panel-default" id="table-tna">
              <div class="panel-heading main-color-bg">
                <h3 class="panel-title">Edit Kategori</h3>
              </div>
              <div class="panel-body">
                <p style="color:#ff6666"> <?php echo $this->session->flashdata('pesan') ?></p>
                <?php foreach($data as $d) {?>
                <?php } ?>
                <form action="<?php echo base_url('admin/kategori_edit/'.$d['id_kategori']).'/y';?>" method="POST">
                  <div class="row">
                    <!-- <div class="col-sm-12">
                      <img style="margin: auto; margin-bottom: 10px; width: 150px;height: 200px;" src="<?php echo base_url().$d['image'] ?>"alt="Foto" class="img-responsive foto-profil">
                    </div> --> 
                  </div>
                  <div class="row">
                    <div class="col-sm-7 form-horizontal">
                      <div class="form-group">
                        <label for="inputNamaLengkap" class="col-sm-4 control-label">Nama Kategori</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" id="inputNamaLengkap" name="nama_kategori" value="<?php echo $d['nama_kategori'] ?>" required>
                        </div>
                      </div>                                          
                      <hr>
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group">
                            <button type="button" onclick="goBack()" class="btn btn-default">Back</button>
                            <button type="submit" class="btn btn-primary pull-right">Submit</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- END MAIN -->
      <?php include 'footer.php'; ?>
