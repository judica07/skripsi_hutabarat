<?php include 'header.php'; ?>
<!-- HEADER -->
<header id="main-header">
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <h2><span class="lnr lnr-bullhorn" ></span> Gerai</h2>
      </div>
    </div>
  </div>
</header>
<!-- END HEADER -->
<!-- MAIN -->
<section id="main-content">
  <div class="container">
    <div class="row">
      <?php include 'sidebar.php'; ?>
      <div class="col-md-9">
        <div class="row">
          <div class="col-md-12">
            <!-- Events -->
            <div class="panel panel-default" id="table-event">
              <div class="panel-heading main-color-bg">
                <h3 class="panel-title">Gerai</h3>
                <a href="<?php echo base_url('admin/gerai_tambah')?>" class="btn btn-default btn-xs pull-right btn-create"><span class="fa fa-plus" aria-hidden="true"></span> </a>
              </div>
              <div class="panel-body">
                <p style="color:#ff6666"> <?php echo $this->session->flashdata('pesan') ?></p>
                <table id="table-event-1" class="table table-striped table-bordered" width="100%">
                  <thead>
                    <tr>
                      <th>NO</th>
                      <th>Nama Minimarket</th>
                      <th>Alamat Minimarket</th>
                      <th>Telepon</th>
                      <th>Kota</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $x=1; foreach ($data as $d) {?>
                      <tr>
                        <td><?php echo $x ?></td>
                        <td><?php echo $d['nama_minimarket']?></td>
                        <td><?php echo $d['alamat_minimarket']?></td>
                        <td><?php echo $d['telp']?></td>
                        <td><?php echo $d['kota']?></td>
                        <td>
                          <a href="<?php echo base_url('admin/gerai/').$d['market_id'];?>" class="btn btn-default btn-sm"><span class="lnr lnr-pencil">Detail</span> </a>
                          <a href="<?php echo base_url('admin/hapus_gerai/').$d['market_id'];?>" onclick="return confirm('Yakin Ingin Menghapus Gerai ini ?')" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-remove">Hapus</span> </a>

                          <a href="<?php echo base_url('admin/tambah_barang1/').$d['market_id'];?>" class="btn btn-default btn-sm"><span class="lnr lnr-pencil">tambah barang</span> </a>

                        </td>
                      </tr>
                      <?php $x++;};?>
                    </tbody>

                  </table>
                </div>
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- END MAIN -->
    <?php include 'footer.php'; ?>
