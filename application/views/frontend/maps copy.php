<!-- breadcrumb start-->
<section class="breadcrumb breadcrumb_bg">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8">
          <div class="breadcrumb_iner">
            <div class="breadcrumb_iner_item">
              <h2><?php echo $breadcrumb; ?></h2>
              <!-- <p>Home <span>-</span> Shop Single</p> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
    <!-- breadcrumb start-->
    
    <section class="sample-text-area">
		<div class="container box_1170">
			<h3 class="text-heading">Text Sample</h3>
			
            <div id="map" style="height: 400px;"></div>
		</div>
	</section>