<!-- breadcrumb start-->
<section class="breadcrumb breadcrumb_bg">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8">
          <div class="breadcrumb_iner">
            <div class="breadcrumb_iner_item">
              <!-- <h2><?php echo $breadcrumb; ?></h2> -->
              <!-- <h2>Shop Category</h2> -->
              <!-- <p>Home <span>-</span> Shop Single</p> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
    <!-- breadcrumb start-->
    
    <section class="cat_product_area section_padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="left_sidebar_area">
                        <aside class="left_widgets p_filter_widgets">
                            <div class="l_w_title">
                                <h3>Browse Categories</h3>
                            </div>
                            <div class="widgets_inner">
                                <ul class="list"><?php
                                foreach ($kategori->result_array() as $d)
                                { 
                                    
                                    ?>
                                
                                    <li>
                                        <a href="<?php echo base_url()?>frontend/kategori/<?php echo $d['id_kategori'] ?>"><?php echo $d['nama_kategori']?></a>
                                        <a href="<?php echo base_url()?>frontend/kategori/<?php echo $d['id_kategori'] ?>"><span><?php echo $d['tes'] ?></span></a>
                                    </li>
                                  
                                <?php 
                                } ?>
                                </ul>
                            </div>
                        </aside>

                      

                      

                      
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="product_top_bar d-flex justify-content-between align-items-center">
                               
                                
                                <!-- <div class="single_product_menu d-flex">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="search"
                                            aria-describedby="inputGroupPrepend">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroupPrepend"><i
                                                    class="ti-search"></i></span>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>


                    <div class="row align-items-center latest_product_inner">
                    <?php
                    if ($product->num_rows() > 0) 
                    {
                        foreach ($product->result_array() as $d)
                        {?>

                        <div class="col-lg-4 col-sm-6">
                            <div class="single_product_item">
                                <img src="<?php echo base_url()?>file_upload/<?php echo $d['image'] ?>" alt="" width="250px" height="250px">
                                <div class="single_product_text">
                                    <h4><?php echo $d['nama_barang']?></h4>
                                    <h3> <?php echo $d['nama_minimarket']?></h3>
                                    <h3><?php echo "harga Normal ", "Rp ".number_format($d['harga_barang'],2,",",".")?></h3>
                                    <h3> <?php echo "harga Promo ", "Rp ".number_format($d['harga_promo'],2,",",".")?></h3>
                                       <?php

                           

                                        if ($d['akhir'] < date("Y-m-d")) {
                                            ?>
                                            <p style="color:#ff6666"> <?php echo  "promo berakhir" ?></p><?php
                                        }
                                        else 
                                        {
                                            ?>
                                             <a href="<?php echo base_url('frontend/product_detail/'.$d['id_barang']) ?>" class="add_cart">Open<i class="ti-heart"></i></a>
                                        <?php 
                                        }

                                    ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                       
                      
                        <?php 
                            }
                        
                        
                    }
                    else
                    {
                    echo "data Tidak Ada";
                    }?>   
                        <div class="col-lg-12">
                            <div class="pageination">
                                <nav aria-label="Page navigation example">
                                    <ul class="pagination custom-pagination justify-content-center">
                                    <!-- <div class="custom-pagination justify-content-center"> -->
                                        <!-- <li class="page-item">
                                            <a class="page-link" href="#" aria-label="Previous">
                                                <i class="ti-angle-double-left"></i>
                                            </a>
                                        </li>
                                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item"><a class="page-link" href="#">4</a></li>
                                        <li class="page-item"><a class="page-link" href="#">5</a></li>
                                        <li class="page-item"><a class="page-link" href="#">6</a></li>
                                        <li class="page-item">
                                            <a class="page-link" href="#" aria-label="Next">
                                                <i class="ti-angle-double-right"></i>
                                            </a>
                                        </li> -->
                                       <?php echo $this->pagination->create_links(); ?>
                                    <!-- </div> -->
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>