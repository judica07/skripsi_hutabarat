<script type="text/javascript">
var markers = JSON.parse(`<?php echo ($peta); ?>`);
console.log(markers);
  function initMap() {
    var bounds = new google.maps.LatLngBounds();
    // var locations = 
    // [
    //   ['Bondi Beach', -33.890542, 151.274856, 4],
    //   ['Coogee Beach', -33.923036, 151.259052, 5],
    //   ['Cronulla Beach', -34.028249, 151.157507, 3],
    //   ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
    //   ['Maroubra Beach', -33.950198, 151.259302, 1]
    // ];
    var locations =  JSON.parse('<?php echo ($peta); ?>');

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 12,
    //   center: new google.maps.LatLng(-33.92, 151.25),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });


    var infoWindow = new google.maps.InfoWindow;
    var image = '<?php echo base_url(); ?>marker/shop.png';
    var pin = '<?php echo base_url(); ?>marker/placeholder.png';
    

     // Try HTML5 geolocation.
     if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            var peepsMarker = new google.maps.Marker({
              position: pos,
              map: map,
              animation: google.maps.Animation.BOUNCE,
              icon: pin
          });
            infoWindow.setPosition(pos);
            infoWindow.setContent(peepsMarker);
        // infoWindow.open(map);
        map.setCenter(pos);
    }, function() {
        handleLocationError(true, infoWindow, map.getCenter());
    });

    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }



    //marker banyak
    var marker, i;

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map,
        icon : image
      });

      
      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          var content = '<h3>'  + locations[i][0] + '</h3>' + 
            '<p align="center"><a href="gerai_detail/' + locations[i][3] + '" class="link_detail btn btn-primary">Lihat Detail</a>';
          
            infoWindow.setContent(content);
            infoWindow.open(map, marker);
        }
      })(marker, i));
    }
  }
  function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
                            'Error: The Geolocation service failed.' :
                            'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
    }
  </script>