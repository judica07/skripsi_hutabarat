<section class="breadcrumb breadcrumb_bg">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8">
          <div class="breadcrumb_iner">
            <div class="breadcrumb_iner_item">
              <!-- <h2><?php echo $breadcrumb; ?></h2> -->
              <!-- <h2>Shop Category</h2> -->
              <!-- <p>Home <span>-</span> Shop Single</p> -->
              <!-- <p>Home <span>-</span> Shop Single</p> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php if (isset($product_d))
{
?>
  <div class="product_image_area section_padding">
    <div class="container">
      <div class="row s_product_inner justify-content-between">
        <div class="col-lg-7 col-xl-7">
          <div class="product_slider_img">
           
              <div data-thumb="img/product/single-product/product_1.png">
                <img src="<?php echo base_url()?>file_upload/<?php echo $product_d['image'] ?>"width="450px" height="450px" />
              </div>
             
           
          </div>
        </div>
        <div class="col-lg-5 col-xl-4">
          <div class="s_product_text">
            <h3><?php echo $product_d['nama_barang']?></h3>
            <h2>Harga : <?php echo "Rp. " .number_format($product_d['harga_barang'],2,",",".")?></h2>
            <h2>Promo : <?php echo "Rp. ".number_format($product_d['harga_promo'],2,",",".")?></h2>
            <ul class="list">
              <li>
                <a class="active" href="<?php echo base_url()?>frontend/kategori/<?php echo $product_d['id_kategori'] ?>">
                  <span>Kategori</span> : <?php echo $product_d['nama_kategori']?></a>
                  
              </li>
              <!-- <li>
                <a href="#"> <span>Availibility</span> : In Stock</a>
              </li> -->
            </ul>
            <p>
            <?php echo $product_d['keterangan']?>
            </p>
            <!-- <div class="card_area d-flex justify-content-between align-items-center">
              <div class="product_count">
                <span class="inumber-decrement"> <i class="ti-minus"></i></span>
                <input class="input-number" type="text" value="1" min="0" max="10">
                <span class="number-increment"> <i class="ti-plus"></i></span>
              </div>
              <a href="#" class="btn_3">add to cart</a>
              <a href="#" class="like_us"> <i class="ti-heart"></i> </a>
            </div> -->
          </div>
        </div>
      </div>
    </div>
  </div>
    
  <?php
}

?>
<section class="sample-text-area">
		<div class="container box_1170">
			<h3 class="text-heading"></h3>
			<a href="javascript:void(0)" onclick="showRoute()" class="btn btn-info btn-sm"> <span class="glyphicon glyphicon-search"></span> Tampilkan Rute </a>
            <div id="map" style="height: 400px;"></div>
		</div>
	</section>

  