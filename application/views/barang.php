<?php include 'header_1.php'; ?>
<!-- HEADER -->
<header id="main-header">
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <h2><span class="lnr lnr-bullhorn" ></span> Barang</h2>
      </div>
    </div>
  </div>
</header>
<!-- END HEADER -->
<!-- MAIN -->
<section id="main-content">
  <div class="container">
    <div class="row">
      <?php include 'sidebar_menu.php'; ?>
      <div class="col-md-9">
        <div class="row">
          <div class="col-md-12">
            <!-- Events -->

            <div class="panel panel-default" id="table-event">
              <div class="panel-heading main-color-bg">
                <a href="<?php echo base_url('dashboard/barang_tambah')?>" class="btn btn-default btn-xs pull-right btn-create"><span class="fa fa-plus" aria-hidden="true"></span> </a>
                <h3 class="panel-title">barang</h3>
              </div>
              <div class="panel-body">
                <p style="color:#ff6666"> <?php echo $this->session->flashdata('pesan') ?></p>
                <table id="table-event-1" class="table table-striped table-bordered" width="100%">
                  <thead>
                    <tr>
                      <th>NO</th>
                      <th>Market id</th>
                      <th>Nama barang</th>
                      <th>harga barang</th>
                      <th>harga promo</th>
                      <th>status</th>
                      <th>keterangan</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $x=1; foreach ($data as $d) {?>
                      <tr>
                        <td><?php echo $x ?></td>
                        <td><?php echo $d['market_id']?></td>
                        <td><?php echo $d['nama_barang']?></td>
                        <td><?php echo $d['harga_barang']?></td>
                        <td><?php echo $d['harga_promo']?></td>
                        <td>
                            <?php
                                        if ($d['akhir'] < date("Y-m-d")) {
                                          ?>
                                          <p style="color:#ff6666"> <?php echo  "promo berakhir" ?></p><?php
                                            
                                        }
                                        else 
                                        {
                                            
                                             echo "promo berlaku";
                                             ?>
                                        <?php 
                                        }

                                    ?>
                        </td>
                        <td><?php echo $d['keterangan']?></td>
                        <td>
                          <a href="<?php echo base_url('dashboard/edit_barang/').$d['id_barang'];?>" class="btn btn-default btn-sm"><span class="lnr lnr-pencil">Edit</span> </a>
                          <a href="<?php echo base_url('dashboard/hapus_barang/').$d['id_barang'];?>" onclick="return confirm('Yakin Ingin Menghapus Barang ini ?')" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-remove">Hapus</span> </a>
                        </td>
                      </tr>
                      <?php $x++;};?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- END MAIN -->
    <?php include 'footer.php'; ?>
