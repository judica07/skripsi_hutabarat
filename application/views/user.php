<?php include 'header_1.php' ?>
<!-- Header -->
<header id="header">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
       <h2><span class="fa fa-fw fa-user fa-lg"></span> Profile</h2>
     </div>
   </div>
 </div>
</header>
<!-- End Header -->
<!-- Main -->
<section id="main">
  <div class="container">
    <div class="row">
      <?php include 'sidebar_menu.php' ?>
      <!-- Content -->
      <div class="col-md-9" id="main-content">
        <div id="profile-tab">
          <!-- Nav tabs -->
          <ul class="nav nav-pills nav-justified" role="tablist">
            <li role="presentation" class="active"><a href="#dataPribadi" aria-controls="dataPribadi" role="tab" data-toggle="tab">Data Pribadi</a></li>
            <li role="presentation"><a href="#ubahPassword" aria-controls="ubahPassword" role="tab" data-toggle="tab">Ubah Password</a></li>
            <li role="presentation"><a href="#akunSocmed" aria-controls="akunSocmed" role="tab" data-toggle="tab">Akun SocMed</a></li>
            <li role="presentation"><a href="#akunBank" aria-controls="akunBank" role="tab" data-toggle="tab">Akun Bank</a></li>
          </ul>
        </div>
        <div id="profile-content">
          <!-- Tab panes -->
          <?php foreach($data as $d) {?>
          <?php } ?>
          <div class="tab-content">
            <p style="color:#ff6666"> <?php echo $this->session->flashdata('pesan') ?></p>
            <div role="tabpanel" class="tab-pane fade in active" id="dataPribadi">
              <div class="row">
                <div class="col-sm-3">
                  <img src="<?php echo base_url().$d['image'] ?>" alt="Foto" class="img-responsive foto-profil">
                  <?php foreach($membership as $m) {
                    if($d['id_membership']==$m['id']) {?>
                      <img src="<?php echo base_url().$m['image'] ?>" alt="Membership" class="img-responsive member-pin" title="Membership">
                    <?php } }?>
                  </div>
                  <div class="col-sm-9">
                    <form id="form-datapribadi" method="post" action="<?php echo base_url('index.php/dashboard/update_profile/pribadi');?>" class="form-horizontal" enctype="multipart/form-data">
                      <div class="form-group">
                        <label for="nama" class="control-label col-sm-3">Nama Lengkap</label>
                        <div class="col-sm-9">
                          <input type="text" id="nama" class="form-control" name="nama" value="<?php echo $d['nama'] ?>" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="foto_profil" class="control-label col-sm-3">Foto Profil</label>
                        <div class="col-sm-9">
                          <input type="file" id="foto_profil" class="form-control" name="foto_profil">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="email" class="control-label col-sm-3">Email</label>
                        <div class="col-sm-9">
                          <p class="form-control-static" id="email"><?php echo $d['email'] ?></p>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="email" class="control-label col-sm-3">Id Member</label>
                        <div class="col-sm-9">
                          <p class="form-control-static" id="idmember"><?php echo $d['id_member'] ?></p>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="no_ktp" class="control-label col-sm-3">No KTP</label>
                        <div class="col-sm-9">
                          <input type="text" id="no_ktp" class="form-control" name="no_ktp" value="<?php echo $d['ktp'] ?>" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="alamat_1" class="control-label col-sm-3">Alamat 1</label>
                        <div class="col-sm-9">
                          <textarea id="alamat_1" class="form-control" name="alamat_1" required><?php echo $d['alamat1'] ?></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="alamat_2" class="control-label col-sm-3">Alamat 2</label>
                        <div class="col-sm-9">
                          <textarea id="alamat_2" class="form-control" name="alamat_2"  required><?php echo $d['alamat2'] ?></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="kota" class="control-label col-sm-3">Kota</label>
                        <div class="col-sm-9">
                          <input type="text" id="kota" class="form-control" name="kota" value="<?php echo $d['kota'] ?>" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="provinsi" class="control-label col-sm-3">Provinsi</label>
                        <div class="col-sm-9">
                          <input type="text" id="provinsi" class="form-control" name="provinsi" value="<?php echo $d['provinsi'] ?>" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="tlp" class="control-label col-sm-3">No Telepon</label>
                        <div class="col-sm-9">
                          <input type="number" id="tlp" class="form-control" name="tlp" value="<?php echo $d['telp'] ?>" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="wa" class="control-label col-sm-3">No WhatsApp</label>
                        <div class="col-sm-9">
                          <input type="number" id="wa" class="form-control" name="wa" value="<?php echo $d['wa'] ?>" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="link_afiliasi" class="control-label col-sm-3">Link Afiliasi</label>
                        <div class="col-sm-9">
                          <p class="form-control-static" id="link_afiliasi"><?php echo $d['slug'] ?></p>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                          <button type="submit" class="btn btn-af" name="btn-simpanProfil">Simpan</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane fade" id="ubahPassword">
                <form id="form-ubahpassword" class="form-horizontal" method="post" action="<?php echo base_url('index.php/dashboard/update_profile/password');?>">
                  <div class="form-group">
                    <label for="password_sekarang" class="control-label col-sm-3">Password Lama</label>
                    <div class="col-sm-7">
                      <input type="password" id="password_sekarang" class="form-control" name="password_sekarang" placeholder="Password Sekarang" required>
                    </div>
                  </div>
                  <br>
                  <div class="form-group">
                    <label for="password_baru" class="control-label col-sm-3">Password Baru</label>
                    <div class="col-sm-7">
                      <input type="password" id="password_baru" class="form-control" name="password_baru" placeholder="Masukkan Password Baru" value="" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="confirm_password_baru" class="control-label col-sm-3">Konfirmasi Password Baru</label>
                    <div class="col-sm-7">
                      <input type="password" id="confirm_password_baru" class="form-control" name="confirm_password_baru" placeholder="Konfirmasi Password Baru" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-7">
                      <button type="submit" class="btn btn-af" name="btn-simpanPass">Simpan</button>
                    </div>
                  </div>
                </form>
              </div>
              <div role="tabpanel" class="tab-pane fade" id="akunSocmed">
                <form id="form-akunsocmed" class="form-horizontal" action="<?php echo base_url('index.php/dashboard/update_profile/socmed');?>" method="post">
                  <div class="form-group">
                    <label for="facebook" class="control-label col-sm-3">Facebook</label>
                    <div class="col-sm-7">
                      <input type="text" id="facebook" class="form-control" name="facebook" value="<?php echo $d['facebook'] ?>" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="twitter" class="control-label col-sm-3">Twitter</label>
                    <div class="col-sm-7">
                      <input type="text" id="twitter" class="form-control" name="twitter" value="<?php echo $d['twitter'] ?>" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="instagram" class="control-label col-sm-3">Instagram</label>
                    <div class="col-sm-7">
                      <input type="text" id="instagram" class="form-control" name="instagram" value="<?php echo $d['instagram'] ?>" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="linkedin" class="control-label col-sm-3">LinkedIn</label>
                    <div class="col-sm-7">
                      <input type="text" id="linkedin" class="form-control" name="linkedin" value="<?php echo $d['linkedin'] ?>" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputWebsite" class="control-label col-sm-3">Website</label>
                    <div class="col-sm-7">
                      <input type="url" id="inputWebsite" class="form-control" name="website" value="<?php echo $d['website'] ?>" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-7">
                      <button type="submit" class="btn btn-af" name="btn-simpanSocmed">Simpan</button>
                    </div>
                  </div>
                </form>
              </div>
              <div role="tabpanel" class="tab-pane fade" id="akunBank">
                <form class="form-horizontal" id="form-akunbank" action="<?php echo base_url('index.php/dashboard/update_profile/bank');?>" method="post">
                  <div class="form-group">
                    <label for="nama_bank" class="control-label col-sm-3">Nama Bank</label>
                    <div class="col-sm-7">
                      <input type="text" id="nama_bank" class="form-control" name="nama_bank" value="<?php echo $d['bank'] ?>" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="cabang_bank" class="control-label col-sm-3">Cabang Bank</label>
                    <div class="col-sm-7">
                      <input type="text" id="cabang_bank" class="form-control" name="cabang_bank" value="<?php echo $d['cabang_bank'] ?>" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="pemilik_rek" class="control-label col-sm-3">Pemilik Rekening</label>
                    <div class="col-sm-7">
                      <input type="text" id="pemilik_rek" class="form-control" name="pemilik_rek" value="<?php echo $d['pemilik_rek'] ?>" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="rekening_bank" class="control-label col-sm-3">No Rekening</label>
                    <div class="col-sm-7">
                      <input type="number" id="rekening_bank" class="form-control" name="rekening_bank" value="<?php echo $d['rekening_bank'] ?>" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-7">
                      <button type="submit" class="btn btn-af" name="btn-simpanBank">Simpan</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- End Content -->
      </div>
    </div>
  </section>
  <!-- End Main -->
  <?php include 'footer.php' ?>
