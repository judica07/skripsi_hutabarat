<?php include 'header_0.php' ?>
<section id="register">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="well well-lg">
          <h1><span class="fa fa-fw fa-file-text-o fa-lg"></span><strong>Register</strong></h1>
          <hr>
          <p style="color:#ff6666"> <?php echo $this->session->flashdata('pesan') ?></p>
          <form id="registerForm" method="post" action="<?php echo base_url('user/registrasi') ?>"  role="form" enctype="multipart/form-data">
            <div class="row">
              <div class="col-sm-6 form-horizontal">
                <div class="form-group">
                  <label for="nama_lengkap" class="col-sm-4 control-label">Nama Lengkap *</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="username" name="username" placeholder="username" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="alamat_email" class="col-sm-4 control-label">Email *</label>
                  <div class="col-sm-8">
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="foto_profil" class="col-sm-4 control-label">Foto Profil *</label>
                  <div class="col-sm-8">
                    <input type="file" class="form-control" id="foto_profil" name="foto_profil" placeholder="Foto Profil" >
                  </div>
                </div>
                <div class="form-group">
                  <label for="password" class="col-sm-4 control-label">Password *</label>
                  <div class="col-sm-8">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="confirm_password" class="col-sm-4 control-label">Konfirmasi Password *</label>
                  <div class="col-sm-8">
                    <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Konfirmasi Password" required>
                  </div>
                </div> 
              </div>             
            </div>
            <br>
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <button type="danger" class="btn btn-af btn-lg"  onclick="goBack()">Back</button>
                  <button type="submit" class="btn btn-af btn-lg" id="btn-registerSubmit" name="btn-registerSubmit">Submit</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- End Main -->
<?php include 'footer.php' ?>
