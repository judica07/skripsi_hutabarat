<?php include 'header_0.php' ?>
  <section id="success_page">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
        <div class="well well-lg">
          <div class="text-center">
            <h1><i class="fa fa-check fa-3x"></i></h1>
            <h1>Terimakasih Telah Mendaftar!</h1>
            <h3></h3>
            <a href="<?php echo base_url('user') ?>" class="btn btn-af"><span class="fa fa-fw fa-home fa-lg"></span> Homepage</a>
          </div>
        </div>
      </div>
      </div>
    </div>
  </section>
  <!-- End Main -->
  <?php include 'footer.php' ?>
