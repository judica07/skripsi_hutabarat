<?php include 'header_1.php'; ?>
<!-- HEADER -->
<header id="main-header">
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <h2><span class="lnr lnr-bullhorn" ></span> Tamnbah Gerai</h2>
      </div>
    </div>
  </div>
</header>
<!-- END HEADER -->
<!-- MAIN -->
<section id="main-content">
  <div class="container">
    <div class="row">
      <?php include 'sidebar_menu.php'; ?>
      <div class="col-md-9">
        <div class="row">
          <div class="col-md-12">
            <!-- Form Edit Event -->
            <div class="panel panel-default" id="table-tna">
              <div class="panel-heading main-color-bg">
                <h3 class="panel-title">Tambah Gerai</h3>
              </div>
              <div class="panel-body">
                <p style="color:#ff6666"> <?php echo $this->session->flashdata('pesan') ?></p>
                
                <form class="form-horizontal" method="post" action="<?php echo base_url('dashboard/create_gerai');?>" enctype="multipart/form-data">
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">Nama Minimarket</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputEvent" name="nama_minimarket" >
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">Alamat Minimarket</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputEvent" name="alamat_minimarket" >
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">Telepon</label>
                    <div class="col-sm-10">
                      <input type="number" class="form-control" id="inputEvent" name="telp" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">Kota</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputEvent" name="kota" >
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">latitude</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="lat" name="lat" >
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">longitude</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="lng" name="lng" >
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div id="map" style="height: 400px;"></div>
                  </div>   
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <div class="pull-right">

                      <button type="button" class="btn btn-danger" onclick="goBack()" id="btn-createEventCancel">Back</button>
                        <button type="reset" class="btn btn-warning" id="btn-createEventReset" name="submit">Reset</button>
                        <button type="submit" class="btn btn-primary" name="submit">Submit</button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- END MAIN -->
  <script>
    var map, infoWindow;
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -34.397, lng: 150.644},
          zoom: 15
        });
        infoWindow = new google.maps.InfoWindow;

        // Try HTML5 geolocation.
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);
            // infoWindow.setContent('Location found.');
            // infoWindow.open(map);
            map.setCenter(pos);
            var marker = new google.maps.Marker({
              position: pos,
              map: map,
              title: 'Click to zoom',
              draggable:true
            });
            document.getElementById('lat').value = pos.lat;
            document.getElementById('lng').value = pos.lng;


            // console.log(pos.lat);

            marker.addListener('drag', handleEvent);
            marker.addListener('dragend', handleEvent);

            infowindow.open(map, marker);
          }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
          });
        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());
        }
      }

      function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
                              'Error: The Geolocation service failed.' :
                              'Error: Your browser doesn\'t support geolocation.');
        infoWindow.open(map);
      }
      function handleEvent(event) {
        document.getElementById('lat').value = event.latLng.lat();
        document.getElementById('lng').value = event.latLng.lng();
      }
    </script>
  <?php include 'footer.php'; ?>

