<?php include 'header_1.php'; ?>
<!-- HEADER -->
<header id="main-header">
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <h2><span class="lnr lnr-user" ></span> User</small></h2>
      </div>
    </div>
  </div>
</header>
<!-- END HEADER -->
<!-- MAIN -->
<section id="main-content">
  <div class="container">
    <div class="row">
      <?php include 'sidebar_menu.php'; ?>
      <div class="col-md-9">
        <div class="row">
          <div class="col-md-12">
            <!-- Form Edit Event -->
            <div class="panel panel-default" id="table-tna">
              <div class="panel-heading main-color-bg">
                <h3 class="panel-title">Edit Users</h3>
              </div>
              <div class="panel-body" >
                <p style="color:#ff6666"> <?php echo $this->session->flashdata('pesan') ?></p>
                <?php 
                foreach($data as $d) 
                {
                echo form_open_multipart('dashboard/edit_password');

                ?>
                  <div class="row">
                    <!-- <div class="col-sm-12">
                      <img style="margin: auto; margin-bottom: 10px; width: 150px;height: 200px;" src="<?php echo base_url().$d['image'] ?>"alt="Foto" class="img-responsive foto-profil">
                    </div> --> 
                  </div>
                  <div class="row" id="ubah_Password">
                    <div class="col-sm-6 form-horizontal">
                      <div class="form-group">
                    <label for="password_sekarang" class="control-label col-sm-4">Password Lama</label>
                    <div class="col-sm-7">
                      <input type="hidden" name="id" class="form-control" value="<?php echo $d['id'];?>">
                      <input type="password" id="password_sekarang" class="form-control" name="password_sekarang" placeholder="Password Sekarang" required>
                    </div>
                  </div>
                  <br>
                  <div class="form-group">
                    <label for="password_baru" class="control-label col-sm-4">Password Baru</label>
                    <div class="col-sm-7">
                      <input type="password" id="password_baru" class="form-control" name="password_baru" placeholder="Masukkan Password Baru" value="" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="confirm_password_baru" class="control-label col-sm-4">Konfirmasi Password Baru</label>
                    <div class="col-sm-7">
                      <input type="password" id="confirm_password_baru" class="form-control" name="confirm_password_baru" placeholder="Konfirmasi Password Baru" required>
                    </div>
                  </div>
                    </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <button type="button" onclick="goBack()" class="btn btn-default">Back</button>
                        <button type="submit" name="submit" class="btn btn-primary pull-right">Submit</button>
                      </div>
                    </div>
                  </div>
                  <?php
                }
                form_close();

                  ?>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- END MAIN -->
        <?php include 'footer.php'; ?>
