<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct(){
		parent::__construct();
		if ($this->session->userdata('status') == "login" && $this->session->userdata('role') == "1") {
			redirect ('admin');
		}
		$this->load->model('m_user');
		$this->load->helper('string');
	}
	function index(){
		$this->load->view('home');
		if ($this->session->userdata('username') != null) {
			redirect(base_url('index.php/dashboard'));
		}
	}
	function daftar(){
		$this->load->view('register');
	}
	function aksi_login()
	{ 	
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$where = array(
			'email' => $email,
			'password' => $this->m_user->hash_password($email,$password),
			// 'ban'=>0
		);
		$cek = $this->m_user->cek_login("user",$where)->num_rows();
		$data =$this->m_user->cek_login("user",$where);
		foreach ($data->result_array() as $key) {
			$userid=$key['id'];
			$role = $key['role'];
			
		}
		if($cek > 0 && $role =='1'){
			$nama =$this->m_user->cek_login("user",array('id'=>$userid))->result_array();
			$data_session = array(
				'id'=> $userid,
				'username' => $nama[0]['username'],
				'status' => "login",
				'role' => $role
			);
			$this->session->set_userdata($data_session);
			redirect(base_url('admin')); //view admin
			
		} else if($cek > 0 && $role=='2'){
			$nama =$this->m_user->cek_login("user",array('id'=>$userid))->result_array();
			$data_session = array(
				'id'=> $userid,
				'nama' => $nama[0]['username'],
				'username' => $email,
				'status' => "login",
				'role' => $role
			);
			$this->session->set_userdata($data_session);
			redirect(base_url('dashboard')); //view user
		}else{
			$this->session->set_flashdata('pesan','Password atau Username Anda Salah!');
			redirect(base_url('user'));
		}
	}
	function registrasi(){
		$this->load->library('upload');
		$config['upload_path']          = './file_upload/';
		$config['allowed_types']        = 'gif|jpg|png|';
		$config['file_name'] = random_string('alnum',5);
		$this->upload->initialize($config);

		if($_FILES['foto_profil']['size']!=0)
		{
			if ($this->upload->do_upload('foto_profil'))
			{
				$data_image = $this->upload->data();
				$foto_profil=$data_image['file_name'];
				$cek_email=$this->m_user->cek_login('user',array('email'=>$_POST['email']))->num_rows();
				
				if ($cek_email>0) {
					$this->session->set_flashdata('pesan','Maaf Email Sudah Pernah Didaftarkan');
					redirect('user/daftar');
				}
				else{
					$username	= $_POST['username'];
					$email 		= $_POST['email'];
					$password 	= $_POST['confirm_password'];

					$data_user				= array(
						'profile_image' => $foto_profil,
						'username'=>$_POST['username'],
						'email'=>$_POST['email'],
						'password'=> $this->m_user->hash_password($email,$password),
						'role'=>'2'

					);

					$where = array(
						'username' => $email,
						'password' =>  $this->m_user->hash_password($email,$password),
					);
					$data =$this->m_user->cek_login("user",$where);
					foreach ($data->result_array() as $key) {
						$userid=$key['id'];
					}
					$res = $this->m_user->add_user($data_user);

					if ($res) {
					$this->load->view('approve_page'); //ke view halaman terimakasih
				} else {
					$this->session->set_flashdata('pesan','Isi Semua Form Dengan Benar!');
					redirect('user/daftar');
				}
			}
		}
	}

	else{
			
				
				$cek_email=$this->m_user->cek_login('user',array('email'=>$_POST['email']))->num_rows();
				if ($cek_email>0) {
					$this->session->set_flashdata('pesan','Maaf Email Sudah Pernah Didaftarkan');
					redirect('user/daftar');
				}
				else{
					$username	= $_POST['username'];
					$email 		= $_POST['email'];
					$password 	= $_POST['confirm_password'];

					$data_user				= array(
						
						'username'=>$_POST['username'],
						'email'=>$_POST['email'],
						'password'=> $this->m_user->hash_password($email,$password),
						'role'=>'2'

					);

					$where = array(
						'username' => $email,
						'password' =>  $this->m_user->hash_password($email,$password),
					);
					$data =$this->m_user->cek_login("user",$where);
					foreach ($data->result_array() as $key) {
						$userid=$key['id'];
					}
					$res = $this->m_user->add_user($data_user);

					if ($res) {
					$this->load->view('approve_page'); //ke view halaman terimakasih
				} else {
					$this->session->set_flashdata('pesan','Isi Semua Form Dengan Benar!');
					redirect('user/daftar');
				}
			}
		
	}		 
		}

		function logout(){
			$this->session->sess_destroy();
			redirect(base_url('user'));
		}
	}


