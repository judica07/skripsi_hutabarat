<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_gerai');
		$this->load->helper('string');
		$this->load->library('pagination');
	}

	public function home(){
		$data['main_view'] = 'frontend/home.php';
		$this->load->view('frontend/template', $data);
	}
	public function maps(){
		$data['breadcrumb'] = 'Product';
		$data['js'] = 'frontend/js/map.php';
		$data['main_view'] = 'frontend/maps.php';
		// $data['peta'] = $this->m_gerai->getAllData('gerai');
		
		$query = $this->m_gerai->getAllData('gerai');
		//$myData = [];
		

		foreach ($query->result_array() as $p) {
			// $myData = $p;

			$myData[] = [
				$p['nama_minimarket'] , $p['lat'],$p['lng'],$p['market_id']
			  ];   
			 //  $infowindow[] = [
				// "<div class=info_content><h3>".$p['nama_minimarket']."</h3><br><p>".$p['nama_minimarket']."</p></div>"
			 //   ];    
		}
		$data['peta'] = json_encode($myData);

		$this->load->view('frontend/template', $data);
	}
	public function index(){
		$data['breadcrumb'] = 'Product';
		$data['main_view'] = 'frontend/product.php';
		$jumlah = $this->m_gerai->get_barang_user2()->num_rows();
		$config['base_url'] = base_url().'frontend/index/';
		$config['total_rows'] = $jumlah;
		$config['per_page'] = 9;

		$from = $this->uri->segment(3);
		$this->pagination->initialize($config);	

		
		$data['product'] = $this->m_gerai->get_barang_user3($config['per_page'], $from);

		// $data['product'] = $this->m_gerai->get_barang_user(array('disable'=>0))->result_array();
		// $data['kategori'] = $this->db->get('kategori');
		$data['kategori'] =$this->m_gerai->get_jumkat();

		$this->load->view('frontend/template', $data);
	}

	public function cari(){
		$data['breadcrumb'] = 'Product';
		$data['main_view'] = 'frontend/product_search.php';
		$data['kategori'] =$this->m_gerai->get_jumkat();
		$cari = (trim($this->input->post('cari',true)))? trim($this->input->post('cari',true)) : '';
		$jumlah = $this->m_gerai->get_search('barang','nama_barang',$cari)->num_rows();
		

		$config['base_url'] = base_url().'frontend/cari/'.$cari.'/';
		$config['total_rows'] = $jumlah;
		// $config['per_page'] = 9;
		// $config['use_page_numbers'] = FALSE;
		$from = $this->uri->segment(4);
		$config['uri_segment']=4;
		$this->pagination->initialize($config);	

		//var_dump($jumlah,$cari);
		
		$data['product'] = $this->m_gerai->get_search2($config['total_rows'], $from, $cari);
		//var_dump($from);
		// $data['product'] = $this->m_gerai->get_barang_user(array('disable'=>0))->result_array();
		// $data['kategori'] = $this->db->get('kategori');
		// $data['kategori'] =$this->m_gerai->get_jumkat();

		$this->load->view('frontend/template', $data);
	





		// var_dump($cari);	
		
	}
	
	function geraifront($market_id=null){

		$data['breadcrumb'] = 'gerai';
		$this->load->library('pagination');
		$data['main_view'] = 'frontend/geraifront.php';
		$jumlah = $this->m_gerai->get_gerai_user()->num_rows();
		$config['base_url'] = base_url().'frontend/geraifront/';
		$config['total_rows'] = $jumlah;
		$config['per_page'] = 9;

		$from = $this->uri->segment(3);
		$this->pagination->initialize($config);	

		
		$data['geraifront'] = $this->m_gerai->get_gerai_user2($config['per_page'], $from);

		// $data['product'] = $this->m_gerai->get_barang_user(array('disable'=>0))->result_array();
		// $data['kategori'] = $this->db->get('kategori');
		$data['kategori'] =$this->m_gerai->get_jumkat();

		$this->load->view('frontend/template', $data);
	}

	public function kategori($id){
		$data['breadcrumb'] = 'Category';
		$data['main_view'] = 'frontend/kategori.php';
		
		$jumlah = $this->m_gerai->get_kategori_id($id)->num_rows();
		$config['base_url'] = base_url().'frontend/kategori/'.$id.'/';
		$config['total_rows'] = $jumlah;
		$config['per_page'] = 9;

		$from = $this->uri->segment(4);
		$this->pagination->initialize($config);	

		
		$data['product'] = $this->m_gerai->get_kategori_id2($config['per_page'], $from, $id);

		// $data['product'] = $this->m_gerai->get_barang_user(array('disable'=>0))->result_array();
		// $data['kategori'] = $this->db->get('kategori');
		$data['kategori'] =$this->m_gerai->get_jumkat();

		$this->load->view('frontend/template', $data);
		
	}
	public function gerai_detail($market_id){
		//$data['breadcrumb'] = 'Gerai Detail';
		$data['main_view'] = 'frontend/gerai_detail.php';
		
		$jumlah = $this->m_gerai->get_gerai_id($market_id)->num_rows();
		//$config['base_url'] = base_url().'frontend/gerai_detail/';
		$config['base_url'] = base_url().'frontend/gerai_detail/'.$market_id.'/';
		$config['total_rows'] = $jumlah;
		$config['use_page_numbers']= FALSE;
		//$offset = $this->uri->segment(4, 0);
		$config['per_page'] = 9;

		$from = $this->uri->segment(4);
		$config['uri_segment'] = 4;
		$this->pagination->initialize($config);	

		
		$data['gerai_detail'] = $this->m_gerai->get_gerai_id2($config['per_page'], $from, $market_id);
		//var_dump($jumlah);
		// $data['product'] = $this->m_gerai->get_barang_user(array('disable'=>0))->result_array();
		// $data['kategori'] = $this->db->get('kategori');
		$data['kategori'] =$this->m_gerai->get_jumkat();

		$this->load->view('frontend/template', $data);
		
	}
	function product_detail($id_barang){
		$data['breadcrumb'] = 'Product Detail';
		$data['js'] = 'frontend/js/mapArah.php';
		$data['main_view'] = 'frontend/product_detail.php';

		$query = $this->m_gerai->get_barang_admin($id_barang);
		$data['product_d'] = $this->m_gerai->get_barang_admin($id_barang)->row_array();
		
		$myData = [];
		

		foreach ($query->result_array() as $p) {

			  $myData[] = [
				$p['nama_minimarket'] , $p['lat'],$p['lng']
			  ];   
		
		}
		$data['peta'] = json_encode($myData);
		$data['infowindow'] = json_encode($myData);
		// var_dump(json_encode($myData));



		$this->load->view('frontend/template',$data);
		
		
	}


	

	
}


